export interface BetItem {
  id: string;
  teams: BetTeam[];
  draw: number;
}

export interface BetTeam {
  name: string;
  win: number;
}

export interface Bets {
  items: BetItem[];
}
