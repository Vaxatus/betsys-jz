import { LitElement } from 'lit-element';
import { fromEvent, Observable } from 'rxjs';
import { BetItem, Bets } from '../../types_interfaces';
import { html, nothing } from 'lit-html';

const config = {
  url: "localhost:3000"
}

export class BetCommunicationController extends LitElement {

  private socket: SocketIOClient.Socket = {} as SocketIOClient.Socket;
  private bets: Bets = {} as Bets

  init () {
    this.socket = io(config.url);
    return this
  }

  start (): void {
    fetch(`http://${config.url}/pulling/start?rate=1`)
  }

  stop (): void {
    fetch(`http://${config.url}/pulling/stop`)
  }

  private onReceive(): Observable<BetItem> {
    return fromEvent(this.socket, 'bet-updated');
  }

  disconnectedCallback(): void {
    super.disconnectedCallback();
    this.socket.disconnect();
  }

  firstUpdated () {
    const observable = this.onReceive();
    observable.subscribe((item: BetItem) => {
      this.dispatchEvent(new CustomEvent('getBets', { detail: {
          items: item
        } }))
    });
  }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  render() { return nothing; }
}

