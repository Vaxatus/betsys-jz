import { html, LitElement, property } from 'lit-element';
// eslint-disable-next-line import/extensions
import { BetItem, Bets } from '../../types_interfaces';
// eslint-disable-next-line import/extensions
import '../../bet-item-row/bet-item-row';
import { nothing } from 'lit-html';

export class BetDashboard extends LitElement {

  @property({type: Object}) bets = {} as Bets;
  @property({type: Function}) start = () => { console.log("onStart") };
  @property({type: Function}) stop = () => { console.log("onStop") };

  getBeats (items:any) {
    return items.length ?
    items.map((item:BetItem) =>
      html`<bet-item-row .home=${(item.teams[0])} .away=${item.teams[1]} .draw=${item.draw} ></bet-item-row>`)
      : nothing
  }

  render() {
    const { bets, getBeats } = this
    return html`
      <nav>
      <h1>NAZAWA WLASNA</h1>
      <button @click=${this.start}>START</button>
      <button @click=${this.stop}>STOP</button>
      </nav>
      <main>
        <ul>
          ${getBeats(bets)}
        </ul>
      </main>
    `;
  }
}
