import { LitElement, html, property } from 'lit-element';
import '../src/components/bet-communication-controller/bet-communication-controller';
import '../src/components/bet-dashboard/bet-dashboard';
//eslint-disable-next-line import/extensions
import { Bets } from './components/types_interfaces';

export class BetsysJz extends LitElement {

  private readonly controller: any;
  private readonly eventHandlers: any[] = [];
  @property({ type: Object }) bets = {} as Bets;


  constructor() {
    super();
    this.controller = document.createElement('bet-communication-controller')
    this.controller.init()
    this.registerHandlers()
  }

  registerHandlers() {
    const handler = (event:CustomEvent) => { this.bets = event.detail.items }
    this.eventHandlers.push(handler)
    this.controller.addEventListener('getBets', handler)
  }

  unRegisterHandler() {
    this.eventHandlers.forEach(handler => {
      this.controller.removeEventListener('getBets', handler)
    })
  }

  disconnectedCallback() {
    super.disconnectedCallback();
    this.unRegisterHandler()
  }

  render() {
    return html`
      <main>
         ${this.controller}
        <bet-dashboard
          .bets=${this.bets}
          .start=${() => this.controller.start()}
          .stop=${() => this.controller.stop()}
          >
      </bet-dashboard>
      </main>
    `;
  }

}
